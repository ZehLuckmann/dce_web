from flask import Blueprint, render_template, request, redirect, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from app.models.tables import Curso
from app.models.forms import CursoForm
from app.ext.db import db
from flask_login import login_user, logout_user, login_required, current_user
from config import UPLOAD_FOLDER
import os, binascii, json
from flask import jsonify, make_response
import qrcode, datetime

bp_app = Blueprint("curso", __name__)
route_prefix = "/curso"

@bp_app.route(route_prefix+"/cadastrar", methods=["GET", "POST"])
@login_required
def cadastrar():
    form = CursoForm()
    if form.validate_on_submit():
        p = Curso()
        form.populate_obj(p)
        db.session.add(p)
        db.session.commit()
        db.session.refresh(p)
        return redirect(url_for("home.home"))
    
    return render_template("curso/cadastrar.html", form=form, action=url_for('curso.cadastrar'))

@bp_app.route(route_prefix+"/consultar", methods=["GET", "POST"])
@login_required
def consultar():
    form = CursoForm()
    curso = None
    if form.validate_on_submit():
        curso = Curso.query.filter_by(token=form.token.data).first()

    return render_template("curso/consultar.html", form=form, curso=curso, action=url_for('curso.consultar'))


@bp_app.route(route_prefix+"/lista")
@login_required
def lista():
    cursos = Curso.query.all()
    return render_template("curso/lista.html", cursos=cursos)

@bp_app.route(route_prefix+"/excluir/<int:id>")
@login_required
def excluir(id):
    curso = Curso.query.filter_by(id=id).first()
    db.session.delete(curso)
    db.session.commit()

    return redirect(url_for("curso.lista"))

def configure(app):
    app.register_blueprint(bp_app)
