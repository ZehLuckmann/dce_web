from email.policy import default
from app.ext.db import db
from datetime import datetime
from datetime import date
from app.models.utils import daterange, STATUS_CARTEIRINHA
from flask_login import UserMixin
from flask import url_for
from os.path import exists
from config import UPLOAD_FOLDER
from sqlalchemy.orm.session import object_session 
import smtplib
import email.message
from binascii import hexlify
import qrcode, os, datetime
from datetime import datetime, date

class Usuario(UserMixin, db.Model):
    __tablename__ = "usuario"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    login = db.Column(db.String(100), unique=True)
    senha = db.Column(db.String(100))
    nome = db.Column(db.String(1000))

class Universidade(db.Model):
    __tablename__ = "universidade"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nome = db.Column(db.String(100))
    
class Curso(db.Model):
    __tablename__ = "curso"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nome = db.Column(db.String(100))
    
class Carteirinha(db.Model):
    __tablename__ = "carteirinha"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(100))

    token = db.Column(db.String(10))
    nome = db.Column(db.String(100))
    sobrenome = db.Column(db.String(100))
    matricula = db.Column(db.String(100))
    cpf = db.Column(db.String(100))
    data_nascimento = db.Column(db.Date)

    universidade_id = db.Column(db.Integer, db.ForeignKey('universidade.id'))
    universidade = db.relationship("Universidade", backref=db.backref("universidade", uselist=False))
    
    curso_id = db.Column(db.Integer, db.ForeignKey('curso.id'))
    curso = db.relationship("Curso", backref=db.backref("curso", uselist=False))

    data_validade = db.Column(db.Date)
    codigo_serie = db.Column(db.String(100))
    codigo_uso = db.Column(db.String(100))

    status = db.Column(db.String(0), default="0")
    observacao = db.Column(db.String(100))

    @property
    def status_str(self):
        r = [item for item in STATUS_CARTEIRINHA if self.status in item]
        return r[0][1]
    
    def enviar_email(self):
        corpo_email = f"""
        Ola {self.nome}
        Sua carteirinha esta com o status X e foi emitida no dia X, o prazo para aprovacao e disponibilidade da carteirinha digital e de Y dias.
        O seu token para acesso a carteira digital e para consulta do status da emissao e {self.token} 

        Em caso de duvidas entre em contato com o DCE pelos canais de contato

        DCE Digital
        """
       
        msg =  email.message.Message()
        msg['Subject'] = "Solicitacao de carteirinha - DCE Digital"
        msg['From'] = 'dcedigital.comunicacao@gmail.com'
        msg['To'] = 'joseh.luckmann@gmail.com'
        password = 'awyywrroyknodokd' 
        msg.add_header('Content-Type', 'text/html')
        msg.set_payload(corpo_email )

        s = smtplib.SMTP('smtp.gmail.com: 587')
        s.starttls()
        s.login(msg['From'], password)
        s.sendmail(msg['From'], [msg['To']], msg.as_string().encode('utf-8'))

    def aprovar(self):
        self.data_validade = date(datetime.now().year+1, 4, 30 )
        self.codigo_serie = hexlify(os.urandom(8)).decode()
        self.codigo_uso = hexlify(os.urandom(8)).decode()
        self.status = "1"
        qr = qrcode.make(self.codigo_serie)
        qr.save(f"{UPLOAD_FOLDER}/carteirinhas/qrcodes/{str(self.id)}.png")
    
    def reprovar(self):
        self.data_validade = None
        self.codigo_serie = None
        self.codigo_uso = None
        self.status = "2"

    @property
    def foto_url(self):
        url = url_for('static', filename=f'uploads/carteirinhas/fotos/{self.id}.png')
        if not exists(f"{UPLOAD_FOLDER}/carteirinhas/fotos/{str(self.id)}.png"):
            url = url_for('static', filename=f'images/default-avatar.png')
        return url

    @property
    def qrcode_url(self):
        url = url_for('static', filename=f'uploads/carteirinhas/qrcodes/{self.id}.png')
        if not exists(f"{UPLOAD_FOLDER}/carteirinhas/qrcodes/{str(self.id)}.png"):
            url = ""
        return url
