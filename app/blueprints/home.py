from flask import Blueprint, render_template, request, redirect, url_for

from app.ext.db import db
from flask_login import login_required

bp_app = Blueprint("home", __name__)


@bp_app.route("/")
@bp_app.route("/home")
def home():
    return render_template("index.html")

def configure(app):
    app.register_blueprint(bp_app)
