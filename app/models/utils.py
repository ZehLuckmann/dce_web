from datetime import timedelta, datetime

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

def inicio_semestre_atual():
    return datetime(datetime.now().year, 1, 1) if datetime.now().month <=6 else datetime(datetime.now().year, 7, 1)

def fim_semestre_atual():
    return datetime(datetime.now().year, 6, 30) if datetime.now().month <=6 else datetime(datetime.now().year, 12, 31)

#Constantes
STATUS_CARTEIRINHA = [
    ("0", "Aguardando Aprovação"),
    ("1", "Aprovada"), 
    ("2", "Reprovada"), 
    ("3", "Vencida")]


ROLES = [
    ("coordenador", "Coordenador"), 
    ("responsavel", "Responsavel"), ]

CSS_CLASS_FIELD = "form-control"