from flask import Blueprint, render_template, request, redirect, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from app.models.tables import Universidade
from app.models.forms import UniversidadeForm
from app.ext.db import db
from flask_login import login_user, logout_user, login_required, current_user
from config import UPLOAD_FOLDER
import os, binascii, json
from flask import jsonify, make_response
import qrcode, datetime

bp_app = Blueprint("universidade", __name__)
route_prefix = "/universidade"

@bp_app.route(route_prefix+"/cadastrar", methods=["GET", "POST"])
@login_required
def cadastrar():
    form = UniversidadeForm()
    if form.validate_on_submit():
        p = Universidade()
        form.populate_obj(p)
        db.session.add(p)
        db.session.commit()
        db.session.refresh(p)
        return redirect(url_for("home.home"))
    
    return render_template("universidade/cadastrar.html", form=form, action=url_for('universidade.cadastrar'))

@bp_app.route(route_prefix+"/consultar", methods=["GET", "POST"])
@login_required
def consultar():
    form = UniversidadeForm()
    universidade = None
    if form.validate_on_submit():
        universidade = Universidade.query.filter_by(token=form.token.data).first()

    return render_template("universidade/consultar.html", form=form, universidade=universidade, action=url_for('universidade.consultar'))


@bp_app.route(route_prefix+"/lista")
@login_required
def lista():
    universidades = Universidade.query.all()
    return render_template("universidade/lista.html", universidades=universidades)

@bp_app.route(route_prefix+"/excluir/<int:id>")
@login_required
def excluir(id):
    universidade = Universidade.query.filter_by(id=id).first()
    db.session.delete(universidade)
    db.session.commit()

    return redirect(url_for("universidade.lista"))

def configure(app):
    app.register_blueprint(bp_app)
