"""empty message

Revision ID: 159d45234d7a
Revises: 077fe36dcd6f
Create Date: 2023-03-10 00:12:10.204969

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '159d45234d7a'
down_revision = '077fe36dcd6f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('carteirinha', schema=None) as batch_op:
        batch_op.add_column(sa.Column('email', sa.String(length=100), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('carteirinha', schema=None) as batch_op:
        batch_op.drop_column('email')

    # ### end Alembic commands ###
