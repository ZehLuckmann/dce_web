import os
from config import UPLOAD_FOLDER

def configure(app):
    folder = f"{UPLOAD_FOLDER}/carteirinhas/fotos/"
    if not os.path.exists(folder):
        os.makedirs(folder)

    folder = f"{UPLOAD_FOLDER}/carteirinhas/qrcodes/"
    if not os.path.exists(folder):
        os.makedirs(folder)