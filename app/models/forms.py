from flask_wtf import FlaskForm
from wtforms import StringField, DateField, EmailField, SelectField, PasswordField
from wtforms.validators import DataRequired
from config import UPLOAD_FOLDER
from flask_wtf.file import FileField, FileRequired, FileAllowed
from datetime import datetime
from app.models.utils import CSS_CLASS_FIELD, inicio_semestre_atual, fim_semestre_atual
from binascii import hexlify
from os import urandom

class CadastroUsuarioForm(FlaskForm):
    nome = StringField("Nome:", validators=[DataRequired()])
    login = StringField("Login:", validators=[DataRequired()])
    senha = PasswordField("Senha:", validators=[DataRequired()])
    
    def insert_data(self, usuario):
        self.nome.data = usuario.nome
        self.login.data = usuario.login
        self.senha.data = usuario.senha

class LoginForm(FlaskForm):
    login = StringField("Login:", validators=[DataRequired()])
    senha = PasswordField("Senha:", validators=[DataRequired()])

class UniversidadeForm(FlaskForm):
    nome = StringField("Nome:", validators=[DataRequired()])

class CursoForm(FlaskForm):
    nome = StringField("Nome:", validators=[DataRequired()])

class ConsultarCarteirinhaForm(FlaskForm):
    matricula = StringField("Matricula:", validators=[DataRequired()])
    token = StringField("Token:", validators=[DataRequired()])

class SolicitacaoCarteirinhaForm(FlaskForm):
    email = EmailField("Email:", validators=[DataRequired()])
    nome = StringField("Nome:", validators=[DataRequired()])
    sobrenome = StringField("Sobrenome:", validators=[DataRequired()])
    matricula = StringField("Matricula:", validators=[DataRequired()])
    cpf = StringField("CPF:", validators=[DataRequired()])
    data_nascimento = DateField("Data Nascimento:", validators=[DataRequired()], format='%d/%m/%Y', default=datetime.now())
    universidade = SelectField('Universidade:',choices=[], coerce=int, validators=[], render_kw={'class': CSS_CLASS_FIELD + " select2"})
    curso = SelectField('Curso:',choices=[], coerce=int, validators=[], render_kw={'class': CSS_CLASS_FIELD + " select2"})
    foto = FileField(validators=[FileRequired()])

    def populate_obj(self, obj):
        obj.curso_id = self.curso.data 
        obj.universidade_id = self.universidade.data 
        obj.email = self.email.data
        obj.nome = self.nome.data
        obj.sobrenome = self.sobrenome.data
        obj.matricula = self.matricula.data
        obj.cpf = self.cpf.data
        obj.data_nascimento = self.data_nascimento.data
        obj.token = hexlify(urandom(5)).decode()

        if self.foto.data:
            self.foto.data.save(f"{UPLOAD_FOLDER}/carteirinhas/fotos/{str(obj.id)}.png")

        return obj
    
    def insert_data(self, usuario):
        self.nome.data = usuario.nome
        self.sobrenome.data = usuario.sobrenome
        self.email.data = usuario.email
        self.matricula.data = usuario.matricula
        self.cpf.data = usuario.cpf
        self.data_nascimento.data = usuario.data_nascimento
        self.universidade.data = usuario.universidade_id
        self.curso.data = usuario.curso_id
