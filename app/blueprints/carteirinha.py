from flask import Blueprint, render_template, request, redirect, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from app.models.tables import Carteirinha, Universidade, Curso
from app.models.forms import SolicitacaoCarteirinhaForm, ConsultarCarteirinhaForm
from app.ext.db import db
from flask_login import login_user, logout_user, login_required, current_user
import os, json
from flask import jsonify, make_response

bp_app = Blueprint("carteirinha", __name__)
route_prefix = "/carteirinha"

@bp_app.route(route_prefix+"/solicitar", methods=["GET", "POST"])
def solicitar():
    form = SolicitacaoCarteirinhaForm()
    form.universidade.choices = [(x.id, x.nome) for x in Universidade.query.all()]
    form.curso.choices = [(x.id, x.nome) for x in Curso.query.all()]

    if form.validate_on_submit():
        carteirinha = Carteirinha()
        form.populate_obj(carteirinha)
        db.session.add(carteirinha)
        db.session.commit()
        db.session.refresh(carteirinha)
        carteirinha.enviar_email()
        return redirect(url_for("home.home"))
    
    return render_template("carteirinha/solicitar.html", form=form, action=url_for('carteirinha.solicitar'))

@bp_app.route(route_prefix+"/consultar", methods=["GET", "POST"])
def consultar():
    form = ConsultarCarteirinhaForm()
    carteirinha = None
    if form.validate_on_submit():
        carteirinha = Carteirinha.query.filter_by(token=form.token.data).first()

    return render_template("carteirinha/consultar.html", form=form, carteirinha=carteirinha, action=url_for('carteirinha.consultar'))

@bp_app.route(route_prefix+"/consulta/<string:matricula>/<string:token>", methods=["GET"])
def consulta(matricula, token):
    response = jsonify({"id": -1,})
    
    carteirinha = Carteirinha.query.filter_by(matricula=matricula).filter_by(token=token).first()
    if carteirinha: 
        response = jsonify({
            "id": carteirinha.id,
            "nome": carteirinha.nome,
            "sobrenome": carteirinha.sobrenome,
            "cpf": carteirinha.cpf,
            "data_nascimento": carteirinha.data_nascimento.strftime("%d/%m/%Y"),
            "matricula": carteirinha.matricula,
            "universidade": carteirinha.universidade.nome,
            "curso": carteirinha.curso.nome,
            "data_validade": carteirinha.data_validade.strftime("%d/%m/%Y"),
            "codigo_serie": carteirinha.codigo_serie,
            "codigo_uso": carteirinha.codigo_uso,
            "foto_url": request.host_url+carteirinha.foto_url,
            "qrcode_url": request.host_url+carteirinha.qrcode_url,
            "status": "valid",
            })
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@bp_app.route(route_prefix+"/lista")
@login_required
def lista():
    carteirinhas = Carteirinha.query.all()
    return render_template("carteirinha/lista.html", carteirinhas=carteirinhas)

@bp_app.route(route_prefix+"/excluir/<int:id>")
@login_required
def excluir(id):
    carteirinha = Carteirinha.query.filter_by(id=id).first()

    db.session.delete(carteirinha)
    db.session.commit()

    return redirect(url_for("carteirinha.lista"))

@bp_app.route(route_prefix+"/atualizar/<int:id>", methods=["GET", "POST"])
@login_required
def atualizar(id):
    form = SolicitacaoCarteirinhaForm()
    
    form.universidade.choices = [(x.id, x.nome) for x in Universidade.query.all()]
    form.curso.choices = [(x.id, x.nome) for x in Curso.query.all()]

    carteirinha = Carteirinha.query.filter_by(id=id).first()

    
    if form.validate_on_submit():
        form.populate_obj(carteirinha)
        db.session.commit()
        return redirect(url_for("carteirinha.lista"))

    form.insert_data(carteirinha)
    return render_template("carteirinha/solicitar.html", form=form)

@bp_app.route(route_prefix+"/aprovar/<int:id>")
@login_required
def aprovar(id):
    carteirinha = Carteirinha.query.filter_by(id=id).first()
    if carteirinha:
        carteirinha.aprovar()
        db.session.commit()

    return redirect(url_for("carteirinha.lista"))

@bp_app.route(route_prefix+"/reprovar/<int:id>")
@login_required
def reprovar(id):
    carteirinha = Carteirinha.query.filter_by(id=id).first()
    if carteirinha:
        carteirinha.reprovar()
        db.session.commit()

    return redirect(url_for("carteirinha.lista"))

def configure(app):
    app.register_blueprint(bp_app)
